<?php

namespace Yadda\Enso\Newsletter\Controllers;

use Exception;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Validation\Rule;
use Yadda\Enso\Newsletter\Contracts\NewsletterControllerContract;
use Yadda\Enso\Newsletter\Contracts\NewsletterHandlerContract;
use Yadda\Enso\Newsletter\Exceptions\NewsletterException;
use Yadda\Enso\Newsletter\Facades\EnsoNewsletter;

class NewsletterController extends BaseController implements NewsletterControllerContract
{
    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    /**
     * Collection of handlers for the given request
     *
     * @param string $type
     *
     * @return NewsletterHandlerContract
     */
    protected function handler(string $type): NewsletterHandlerContract
    {
        $class = Config::get('enso.newsletter.newsletters.' . $type . '.handler');

        return new $class;
    }

    /**
     * Handle Newsletter store requests
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $handler = $this->handler($request->input('type'));
        } catch (Exception $e) {
            Log::error($e);

            if ($request->ajax() || $request->wantsJson()) {
                return Response::json([
                    'message' => 'Unknown signup form type',
                ], 400);
            } else {
                return Redirect::back()->withInput()->with('message', 'Unknown form type');
            }
        }

        $this->validate(
            $request,
            array_merge(['email' => ['required', 'email']], $handler->rules()),
            $handler->messages()
        );

        try {
            $response = $handler->handle($request->all());
        } catch (NewsletterException $e) {
            Log::error($e);

            return Response::json([
                'message' => $e->getMessage(),
                'errors' => []
            ], $e->getCode());
        } catch (Exception $e) {
            Log::error($e);

            return Response::json([
                'message' => "Something went wrong.",
                'errors' => []
            ], 500);
        }

        if ($response instanceof \Symfony\Component\HttpFoundation\Response) {
            return $response;
        }

        if ($request->ajax() || $request->wantsJson()) {
            return Response::json('ok', 200);
        } else {
            return Redirect::back();
        }
    }
}
