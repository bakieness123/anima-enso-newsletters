<?php

namespace Yadda\Enso\Newsletter\Contracts;

use Illuminate\Http\Request;

interface NewsletterControllerContract
{
    /**
     * Handle Newsletter store requests
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request);
}
