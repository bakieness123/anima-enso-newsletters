<?php

namespace Yadda\Enso\Newsletter\Contracts;

use Illuminate\Database\Eloquent\Model;

interface NewsletterHandlerContract
{
    /**
     * Gets all displayable newsletter data. This should be presented as key-value pairs.
     *
     * @param Model $newsletter
     *
     * @return array
     */
    public function getDisplayableFormData(Model $newsletter): array;

    /**
     * Gets the important Form data for displaying on index pages
     *
     * @param Model $newsletter
     *
     * @return array
     */
    public static function getImportantFormData(Model $newsletter): array;

    /**
     * Handle the request data
     *
     * @param array $request_data
     *
     * @return mixed
     */
    public function handle(array $request_data);

    /**
     * Validation messages
     *
     * @return array
     */
    public function messages(): array;

    /**
     * Validation rules
     *
     * @return array
     */
    public function rules(): array;
}
