<?php

namespace Yadda\Enso\Newsletter\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;

class NewsletterAdminEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $request_data = [])
    {
        $this->email = Arr::get($request_data, 'email');
        $this->data = Arr::except($request_data, ['type', 'email', '_token'], []);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('enso-newsletters::admin-email', [
            'email' => $this->email,
            'data' => $this->data,
        ]);
    }
}
