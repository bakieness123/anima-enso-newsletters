<?php

return [

    /**
     * Whether to show Newsletters in the CMS.
     *
     * If you enable this, you will need to ensure that you have added the
     * crudRoutes and added the database table.
     */
    'enable_admin' => false,

    /**
     * Newsletter definitions
     */
    'newsletters' => [
        // 'example_type' => [
        //     'handler' => '\Class\Of\Handler',
        //     ... type-specific properties
        // ],
    ],

];
