<?php

namespace Yadda\Enso\Newsletter\Traits;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Yadda\Enso\Newsletter\Contracts\NewsletterAdminEmailContract;
use Yadda\Enso\Settings\Facades\EnsoSettings;
use Yadda\Enso\Utilities\Helpers;

trait EmailsAdmins
{
    /**
     * Saves a request to the database
     *
     * @param array $request_data
     *
     * @return NewsletterModelContract
     */
    protected function emailAdmins(array $request_data)
    {
        $admins = array_map('trim', array_filter(explode(',', $this->adminEmailAddresses())));

        if (empty($admins)) {
            Log::warning('Tried to send EnsoNewsletters email to admin but no admin address found.');
            return;
        }

        $mailer = $this->mailerClass();

        Mail::to(Arr::first($admins))
            ->send(new $mailer($request_data));
    }

    /**
     * Get the class of the Mailer to send
     *
     * @return string
     */
    protected function mailerClass()
    {
        return Helpers::getConcreteClass(NewsletterAdminEmailContract::class);
    }

    /**
     * Get a csv string of email addresses to send to
     *
     * @return string
     */
    protected function adminEmailAddresses(): string
    {
        return EnsoSettings::get('administrator-email', '');
    }
}
