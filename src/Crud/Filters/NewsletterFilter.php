<?php

namespace Yadda\Enso\Newsletter\Crud\Filters;

use Yadda\Enso\Crud\Filters\BaseFilters\TextFilter;

class NewsletterFilter extends TextFilter
{
    /**
     * Columns to search in
     *
     * @var array
     */
    protected $columns = [
        'email',
    ];

    /**
     * Label to apply to the filter
     *
     * @var string
     */
    protected $label = 'Email';

    /**
     * Props to apply to the filter
     *
     * @var array
     */
    protected $props = [
        'placeholder' => 'Search...',
        'help-text' => 'Search by submission email',
    ];
}
